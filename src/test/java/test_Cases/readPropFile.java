package test_Cases;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class readPropFile {

	
	public void read(String path) throws IOException {
		
		FileInputStream fis = new FileInputStream(path);
		Properties prop = new Properties();
		prop.load(fis);
		
		String calc = prop.getProperty("calculator");
		
		System.out.println(calc);
	}
}
