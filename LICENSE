package stepDefinitions;

import java.sql.SQLException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import dataProvider.ConfigFileReader;
import helpers.SauceConnection;
import helpers.WebDriverCapabilities;

/**
 * @author banesh
 *
 */

public class BaseDefinitions extends WebDriverCapabilities {

	public String sauceID, remoteTunnel, tunnelID, remoteURL;
	ConfigFileReader config = new ConfigFileReader();

	final static Logger logger = LogManager.getLogger(BaseDefinitions.class);

	@Before()
	public void setUp(Scenario scenario) throws Throwable {
		
		if (config.getExecutionEnvironment().equalsIgnoreCase("local")) {
			String lbrowser = config.getLocalBrowser();
			switch (lbrowser) {
			case "chrome":
				setLocalChromeDriver();
				break;
			case "firefox":
				setLocalFirefoxDriver();
				break;
			case "ie":
				setLocalIEDriver();
				break;
			default:
				setLocalChromeDriver();
				break;
			}
		} else {
			SauceConnection.handleSauceSSLException();
			remoteURL = SauceConnection.fetchSauceTunnel();
			String[] URL = remoteURL.split("&");
			remoteTunnel = URL[0];
			tunnelID = URL[1];
			String rbrowser = config.getRemoteBrowser();
			switch (rbrowser) {
			case "chrome":
				setRemoteChromeBrowser(scenario, remoteTunnel, tunnelID);
				break;
			case "firefox":
				setRemoteFirefoxBrowser(scenario, remoteTunnel, tunnelID);
				break;
			case "ie":
				setRemoteIEBrowser(scenario, remoteTunnel, tunnelID);
				break;
			default:
				setRemoteChromeBrowser(scenario, remoteTunnel, tunnelID);
				break;
			}
		}
	}

	@After()
	public void tearDown(Scenario scenario) throws SQLException, Exception {
		if (config.getExecutionEnvironment().equalsIgnoreCase("local")) {
			closeWebDriver();
		} else {
			embedScreenshot(scenario);
			sauceID = ((RemoteWebDriver) WebDriverCapabilities.driver).getSessionId().toString();
			SauceConnection.updateSauceLabStatus(scenario, sauceID, WebDriverCapabilities.driver);
		}
	}

}

