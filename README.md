package dataProvider;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
	
	private Properties properties;
	private final String propertyFilePath= "src/test/resources/config/config.properties";

	
	public ConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}
	
	public String getExecutionEnvironment(){
		String environment = properties.getProperty("execution_environment");
		if(environment!= null) return environment;
		else throw new RuntimeException("environment not specified in the config.properties file.");		
	}
	
	public long getImplicitlyWait() {		
		String implicitlyWait = properties.getProperty("implicitlyWait");
		if(implicitlyWait != null) return Long.parseLong(implicitlyWait);
		else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file.");		
	}
	
	public String getLocalBrowser() {
		String browser = properties.getProperty("local_browser");
		if(browser != null) return browser;
		else throw new RuntimeException("local_browser not specified in the config.properties file.");
	}
	
	public String getRemoteBrowser() {
		String browser = properties.getProperty("remote_browser");
		if(browser != null) return browser;
		else throw new RuntimeException("remote_browser not specified in the config.properties file.");
	}

}
